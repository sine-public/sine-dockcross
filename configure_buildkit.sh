#!/usr/bin/env bash

docker buildx create --bootstrap --use --name buildkit --driver-opt env.BUILDKIT_STEP_LOG_MAX_SIZE=-1 --driver-opt env.BUILDKIT_STEP_LOG_MAX_SPEED=-1
docker buildx install
