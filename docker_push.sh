#!/usr/bin/env bash

# Tag images with date and Git short hash in addition to revision
TAG=$(git log -1 --date=format:'%Y%m%d' --format='%ad')-$(git rev-parse --short HEAD)

CONTAINERS=("base" \
	"windows-shared-x64" "windows-shared-x64-posix" "windows-shared-x86" \
	"windows-static-x64" "windows-static-x64-posix" "windows-static-x86" \
	"linux-x86_64-full" \
	"linux-arm64-full" "linux-armv8-full")

for CONTAINER in ${CONTAINERS[@]}; do
	docker push sineengineering/dockcross-$CONTAINER:$TAG
	docker push sineengineering/dockcross-$CONTAINER:latest
done
