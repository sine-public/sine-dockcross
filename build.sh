#!/usr/bin/env bash

# File for building all required modified crosscompilers

# Launch the file server if the submodule with it is checked out
if [ -f "./vendor/simple-file-server/server.sh" ]; then
	cd ./vendor/simple-file-server/
	./server.sh
	cd ./../..
fi

CONTAINERS=("base" \
	"windows-shared-x64" "windows-shared-x64-posix" "windows-shared-x86" \
	"windows-static-x64" "windows-static-x64-posix" "windows-static-x86" \
	"linux-x86_64-full" \
	"linux-arm64-full" "linux-armv8-full")

for CONTAINER in ${CONTAINERS[@]}; do
	make $CONTAINER
done
