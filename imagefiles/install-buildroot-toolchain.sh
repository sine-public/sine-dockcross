#!/usr/bin/env bash
set -x
set -e
set -o pipefail

ROOT=${PWD}

usage() { echo "Usage: $0 -c <config-path> -v <version>" 1>&2; exit 1; }

REPO_URL="https://github.com/buildroot/buildroot.git"

FILE_SERVER_URL="http://host.docker.internal:29802"
#FILE_SERVER_URL="http://localhost:29802"
#FILE_SERVER_URL="http://172.17.0.1:29802"

CONFIG_PATH=""
REV="2024.11"
while getopts "c:v:" o; do
  case "${o}" in
  c)
    CONFIG_PATH=$(readlink -f ${OPTARG})
    ;;
  v)
    REV=${OPTARG}
    ;;
  *)
    usage
    ;;
  esac
done
shift $((OPTIND-1))

if [ -z ${CONFIG_PATH} ] || [ ! -f ${CONFIG_PATH} ]; then
  echo "ERROR: Missing config path (-c)."
  usage
fi

if [ -z ${REV} ]; then
  echo "WARNING: No version selected, use default version: $REV (-v)."
fi

CACHE_ARHIVES=("buildroot_downloads_cache.tar.xz" "buildroot_ccache.tar.xz")
if curl --output /dev/null --silent --head --fail --retry 5 --retry-delay 10 "$FILE_SERVER_URL/heartbeat"; then
    echo "File server is available, downloading the caches..."

    for CACHE_ARCHIVE in "${CACHE_ARHIVES[@]}"; do
        CACHE_DIRECTORY="./${CACHE_ARCHIVE%.*.*}"
        mkdir $CACHE_DIRECTORY

        HTTP_STATUS=$(curl -o "./$CACHE_ARCHIVE" -w "%{http_code}" "$FILE_SERVER_URL/file/$CROSS_TRIPLE/$CACHE_ARCHIVE")
        if [ "$HTTP_STATUS" -ne 404 ]; then
            tar -xf "./$CACHE_ARCHIVE" -C $CACHE_DIRECTORY
            rm "./$CACHE_ARCHIVE"
        else
            echo "ERROR: File $CACHE_ARCHIVE not found on server."
            rm "./$CACHE_ARCHIVE"
        fi
    done
else
    echo "File server is not available."
fi

# Clone the repository and checkout the specified revision
git clone "$REPO_URL" --recurse-submodules --remote-submodules #--branch="$REV"
cd buildroot
git checkout "$REV"
mv "$CONFIG_PATH" .config

# Apply the patch to the Buildroot tree to have all the correct packages configured
git apply ./../buildroot.patch
rm ./../buildroot.patch

# Build Buildroot
make

cd ./../
if curl --output /dev/null --silent --head --fail --retry 5 --retry-delay 10 "$FILE_SERVER_URL/heartbeat"; then
    echo "File server is still available, uploading the caches..."

    for CACHE_ARCHIVE in "${CACHE_ARHIVES[@]}"; do
        CACHE_DIRECTORY="./${CACHE_ARCHIVE%.*.*}"
        
        tar -cJf "./$CACHE_ARCHIVE" -C "$CACHE_DIRECTORY" .
        MIME_TYPE=$(file --mime-type -b "./$CACHE_ARCHIVE")
        curl -X PUT -H "Content-Type: $MIME_TYPE" --upload-file "./$CACHE_ARCHIVE" "$FILE_SERVER_URL/file/$CROSS_TRIPLE/$CACHE_ARCHIVE"

        rm "./$CACHE_ARCHIVE"
        rm -rf "$CACHE_DIRECTORY"
    done
else
    echo "File server is still/anymore not available."
fi
